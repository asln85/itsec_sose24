# itsec_SoSe24



## Malware-Praktikum

Es liegen zwei verschiedene Beispiele zu polymorphen Programmen vor: der Polymorph-Ordner enthält ein einfaches Skript, welches nacheinander ein Entschlüsselungsskript auf die Payload (ein einfaches Hello World-Skript), dann diese Payload, und dann ein Verschlüsselungsskript auf die Payload anwendet. Durch das Verschlüsselungsskript sieht die Payload nach jeder Ausführung in Binärdarstellung anders aus.

In PolyTheo liegt ein Programm, was dieses Konzept in einem einzigen Pythonskript umsetzt; Es liegt außerdem eine unverschlüsselte Version vor.


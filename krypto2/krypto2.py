import rsa
import hashlib

def generate_keypair():
    (public_key, private_key) = rsa.newkeys(2048)
    return private_key, public_key

def calculate_hash(message):
    hashed_message = hashlib.sha256(message.encode()).digest()
    return hashed_message

def encrypt_message(message, public_key):
    encrypted_message = rsa.encrypt(message.encode(), public_key)
    return encrypted_message

def decrypt_message(encrypted_message, private_key):
    decrypted_message = rsa.decrypt(encrypted_message, private_key).decode()
    return decrypted_message

def sign_message(message, private_key):
    signature = rsa.sign(message.encode(), private_key, 'SHA-256')
    return signature

def verify_signature(message, signature, public_key):
    try:
        rsa.verify(message.encode(), signature, public_key)
        return True
    except rsa.pkcs1.VerificationError:
        return False


# Generate keypairs
private_key, public_key = generate_keypair()
private_key1, public_key1 = generate_keypair()

message = "Hello, World!"

# Calculate hash
hashed_message = calculate_hash(message)
print(f"SHA-256 Hash: {hashed_message.hex()}")
print("\n")

# Encrypt message
encrypted_message = encrypt_message(message, public_key)
print(f"Encrypted Message: {encrypted_message.hex()}")
print("\n")

# Decrypt message
decrypted_message = decrypt_message(encrypted_message, private_key)
print(f"Decrypted Message: {decrypted_message}")
print("\n")

# Sign message
signature = sign_message(message, private_key)
print(f"Signature: {signature.hex()}")
print("\n")

# Verify signature
is_verifiedT = verify_signature(message, signature, public_key)
print(f"Signature Verified: {is_verifiedT}")
print("\n")

is_verifiedF = verify_signature(message, signature, public_key1)
print(f"Signature Verified: {is_verifiedF}")

